package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/db"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/handler"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/model"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/service"
)

func init() {
	// init database
	defer db.Init()

	// init logger
	log.SetFormatter(&log.TextFormatter{})
	log.SetReportCaller(true)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		log.Info("Log level not specified, set default to: INFO")
		log.SetLevel(log.InfoLevel)
		return
	}
	log.SetLevel(level)
}

func loadSampleData() {
	campaign := model.Campaign{
		Name:               "Spenden für eine bessere Note",
		OrganizerName:      "Geheimnisvolle Ente",
		TargetAmount:       5000,
		DonationMinimum:    10,
		AmountDonatedSoFar: 0,
		Account: model.Account{
			Name:     "Dagobert Duck",
			BankName: "Dagoberts Bank",
			Number:   "DE3414022023",
		},
	}

	campaign.Donations = append(campaign.Donations, model.Donation{
		Amount:           10,
		DonorName:        "Robin Wood",
		ReceiptRequested: true,
		Account: model.Account{
			Name:     "Robin Wood",
			BankName: "Dagoberts Bank",
			Number:   "DE3414022000",
		},
		Status: model.IN_PROCESS,
	})

	service.CreateCampaign(&campaign)
}

func main() {
	loadSampleData()
	port := 8000
	log.Infof("Starting MyAktion API server on port %v.\n", port)
	router := mux.NewRouter()
	router.HandleFunc("/health", handler.Health).Methods("GET")
	router.HandleFunc("/campaigns", handler.GetCampaigns).Methods("GET")
	router.HandleFunc("/campaigns", handler.CreateCampaign).Methods("POST")
	router.HandleFunc("/campaigns/{id}", handler.GetCampaign).Methods("GET")
	router.HandleFunc("/campaigns/{id}", handler.UpdateCampaign).Methods("PUT")
	router.HandleFunc("/campaigns/{id}", handler.DeleteCampaign).Methods("DELETE")
	router.HandleFunc("/campaigns/{id}/donation", handler.AddDonation).Methods("POST")

	if err := http.ListenAndServe(fmt.Sprintf(":%v", port), router); err != nil {
		log.Fatal(err)
	}
}
