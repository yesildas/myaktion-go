package model

type Account struct {
	Name     string `json:"name" gorm:"notNull"`
	BankName string `json:"bankName" gorm:"notNull"`
	Number   string `json:"number" gorm:"notNull"`
}
