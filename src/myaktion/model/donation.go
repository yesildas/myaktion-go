package model

const (
	TRANSFERRED Status = "überwiesen"
	IN_PROCESS  Status = "in Bearbeitung"
)

type Donation struct {
	CampaignID       uint    `json:"campaignId"`
	Amount           float64 `json:"amount" gorm:"notNull"`
	DonorName        string  `json:"donorName" gorm:"notNull"`
	ReceiptRequested bool    `json:"receiptRequested" gorm:"notNull"`
	Account          Account `json:"account" gorm:"embedded; embeddedPrefix:account_; foreignKey: CampaignID; constraint:OnUpdate:CASCADE, OnDelete:CASCADE"`
	Status           Status  `json:"status" gorm:"notNull; type:ENUM('TRANSFERRED','IN_PROCESS')"`
}

type Status string
