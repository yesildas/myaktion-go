package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCreateCampaign(t *testing.T) {
	rr := httptest.NewRecorder()
	req := httptest.NewRequest("POST", "/campaigns", nil)
	handler := http.HandlerFunc(CreateCampaign)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v expected %v", status, http.StatusBadRequest)
	}
}
