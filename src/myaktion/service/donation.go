package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/db"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/model"
)

func AddDonation(campaignId uint, donation *model.Donation) error {
	donation.CampaignID = campaignId
	result := db.DB.Create(donation)
	if result.Error != nil {
		log.Info("Donation creation failed.")
		return result.Error
	}
	log.Info("Successfully stored new donation")
	log.Tracef("Stored: %v", donation)

	return nil
}
