package service

import (
	log "github.com/sirupsen/logrus"

	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/db"
	"gitlab.reutlingen-university.de/yesildas/myaktion-go/src/myaktion/model"
)

var (
	campaignStore map[uint]*model.Campaign
	actCampaignId uint = 1
)

func init() {
	campaignStore = make(map[uint]*model.Campaign)
}

func CreateCampaign(campaign *model.Campaign) error {
	result := db.DB.Create(campaign)
	if result.Error != nil {
		log.Info("Campaign creation failed.")
		return result.Error
	}
	log.Infof("Successfully stored new campaign with ID %v in database.", campaign.ID)
	log.Tracef("Stored: %v", campaign)

	return nil
}

func GetCampaign(id uint) (*model.Campaign, error) {
	var campaign model.Campaign
	result := db.DB.Preload("Donations").First(&campaign, id)
	if result.Error != nil {
		log.Info("Campaign retrieving failed.")
		return nil, result.Error
	}
	log.Infof("Successfully retrieved campaign with ID %v.", campaign.ID)
	log.Tracef("Retrieved: %v", campaign)

	return &campaign, nil
}

func GetCampaigns() ([]model.Campaign, error) {
	var campaigns []model.Campaign
	result := db.DB.Preload("Donations").Find(&campaigns)
	if result.Error != nil {
		return nil, result.Error
	}
	log.Infof("Successfully retrieved %d campaigns.", len(campaigns))
	log.Tracef("Retrieved: %v", campaigns)

	return campaigns, nil
}

func UpdateCampaign(id uint, campaign *model.Campaign) (*model.Campaign, error) {
	var campaignData model.Campaign
	result := db.DB.Preload("Donations").First(&campaignData, id)
	if result.Error != nil {
		log.Info("Campaign retrieving failed.")
		return nil, result.Error
	}
	result = db.DB.Model(&campaignData).Updates(&campaign)
	if result.Error != nil {
		log.Info("Campaign updating failed.")
		return nil, result.Error
	}
	log.Infof("Successfully updated campaign with ID %d.", id)
	log.Tracef("Updated: %v", campaignData)

	return &campaignData, nil
}

func DeleteCampaign(id uint) error {
	result := db.DB.Preload("Donations").Delete(&model.Campaign{}, id)
	if result.Error != nil {
		log.Info("Campaign delete failed.")
		return result.Error
	}

	log.Infof("Successfully deleted campaign with ID %v.", id)
	return nil
}
